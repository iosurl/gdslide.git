//
//  ViewController.m
//  GDpush
//
//  Created by Matin on 2021/7/16.
//

#import "ViewController.h"
#import "SlideView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    SlideView * slideView = [[SlideView alloc] initWithFrame:CGRectMake(0, kScreenHeight-140, kScreenWidth, kScreenHeight-100)];
    slideView.topH = 100;
    [self.view addSubview:slideView];
}


@end
