//
//  SlideView.h
//  GDpush
//
//  Created by Matin on 2021/7/16.
//

#define kScreenWidth           ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight          ([UIScreen mainScreen].bounds.size.height)

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SlideView : UIView

@property (nonatomic,assign) float topH;//上滑后距离顶部的距离

@end

NS_ASSUME_NONNULL_END
