//
//  SceneDelegate.h
//  GDpush
//
//  Created by Matin on 2021/7/16.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

